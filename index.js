
//num1
const getCube1 = (100 ** 3);
console.log (getCube1);


//num2
const getCube = (putNum) => {
    return putNum
}

let getCubeVar = (prompt('(with prompt) Enter a number:'));
let getCubeVarAns = getCubeVar ** 3;

console.log (`The cube of ${getCubeVar} is ${getCubeVarAns} `);

//num3
const address = ["258", "Washington Ave NW", "California", "90011"];

const [StreetNum, City, State, PostCode] = address;

console.log(`I live at ${StreetNum} ${City} ${State}, ${PostCode} `);

//num4

const animal = {
            name: "Lolong",
            species: "saltwater crocodile",
            weight: "1075 kgs",
            measurement: "20 ft 3 in"
        };


const {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

//num5

const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']

characters.forEach(characterArray => {console.log(`${characterArray}`)})

//num6

class Dog {
    constructor (dogName, age, breed){
        this.dogName = dogName;
        this.age = age;
        this.breed = breed;
    }
}

const dog1= new Dog("Ariana", 2, "Aspin");
console.log(dog1);
const dog2= new Dog("Brett", 1, "Chiuaua");
console.log(dog2);
